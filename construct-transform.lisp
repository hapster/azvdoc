;; CONSTRUCTING (ARBITRARY) TIMESPANS
;; ========================================================================

(cl:in-package :azvdoc)

(defun construct-timespan (&optional start end pause place pk ov2 ov1)
  "Constructs an arbitrary timespan with the given parameters.
Applicable for TIMESPAN, DAY, MONTH, QUARTER, YEAR."
  (list :start start
	:end end
	:pause pause
	:place place
	:pk pk
	:ov2 ov2
	:ov1 ov1))

;; TRANSFORM LIST OF TIMESPANS TO DAYSPAN
;; ==================================================================================

(defun get-pause-of-day (list)
  "Given a list of verbose timespans, return the PAUSE duration."
  (let ((one (first list)))
    (cond ((getf one :pause)
	   (compute-hours (getf one :start)
			  (getf one :end)))
	  (t (get-pause-of-day (cdr list))))))

(defun get-start-of-day (list)
  "Given a list of verbose timespans, returns START of day as string.
Uses the START field of the first timespan."
  (getf (first list) :start))

(defun get-end-of-day (list)
    "Given a list of verbose timespans, returns END of day as string.
Uses the END field of the last timespan."
  (getf (first (last list)) :end))

(defun get-places-of-day (list)
  "Given a list of verbose timespans, returns a list of PLACES.
Removes duplicate entries."
  (do* ((x  list  (rest x))
	(e  (first x)  (first x))
	(place-e  (getf e :place)  (getf e :place))
	(result nil))
       ((null x) (remove nil result))
    (unless (member place-e result)
      (push place-e result))))

(defun get-type-of-day (type list)
  "Given AZV type symbol and a list of verbose timespans, return list of AZV."
  (let ((keyword (intern (string-upcase type) "KEYWORD")))
    (cond ((null list) nil)
	  ((getf (car list) keyword)
	   (cons (car list)
		 (get-type-of-day keyword (cdr list))))
	  (t (get-type-of-day keyword (cdr list))))))

(defun timespan-decide-type (ts)
  "Given one timespan, export present type specifier and category.
Comes as a two-element proper list."
  (let ((pk   (getf ts :pk))
	(ov2  (getf ts :ov2))
	(ov1  (getf ts :ov1)))
    (cond (pk  (list (quote pk) pk))
	  (ov2 (list (quote ov2) ov2))
	  (ov1 (list (quote ov1) ov1)))))

(defun compact-one-timespan (ts)
  "Given one timespan, produce its compacted form."
  (let ((start (getf ts :start))
	(end   (getf ts :end))
	(place (getf ts :place))
	(type  (timespan-decide-type ts)))
    (list :place place
	  :soll (car (cdr type))
	  :duration (compute-hours start end))))

(defun compact-all-timespans (list)
  "Given a list of verbose timespans, produce a list of its compacted forms."
  (cond ((null list) nil)
	(t (cons (compact-one-timespan (car list))
		 (compact-all-timespans (cdr list))))))

(defun transform-timespans-to-day (list)
  "Given a list of verbose timespans, produce a DAY record.
First transforms timespans to compact form, then sorts and produces record."
  (let* ((start      (get-start-of-day list))
	 (end        (get-end-of-day list))
	 (pause      (get-pause-of-day list))
	 (places     (get-places-of-day list))
	 (pk         (get-type-of-day 'pk list))
	 (ov2        (get-type-of-day 'ov2 list))
	 (ov1        (get-type-of-day 'ov1 list)))
    (construct-timespan start end pause places
			(compact-all-timespans pk)
			(compact-all-timespans ov2)
			(compact-all-timespans ov1))))

(defun set-one-day-year-array (num array content)
  "Given day-in-year NUM, set specified slot in ARRAY with CONTENT."
  (setf (elt array num) content))

(defun get-list-of-entries-year-array (start end array)
  "Produces a list of entries in the given timespan.
Uses START and END to delimit the timespan, reads ARRAY."
  (cond ((eq start end) nil)
	(t (cons (elt array start)
		 (get-list-of-entries-year-array (+ start 1)
						 end
						 array)))))

;; FIND AND ACCUMULATE SAME ENTRIES
;; ==================================================================================

(defun find-same-entries-one-compacted-ts (ts)
  (do* ((x ts (rest x))
	(e (first x))
	(comp (rest x) (rest x))
	(result nil))
       ((null x) (push e result))
    (let ((place-1 (getf e :place))
	  (soll-1 (getf e :soll))
	  (place-2 (getf (first comp) :place))
	  (soll-2 (getf (first comp) :soll)))
      (when (and (eq place-1 place-2)
		 (eq soll-1 soll-2))
	(push (first comp) result)))))

(defun add-matching-entries-duration (ts)
  (cond ((null ts) 0)
	(t (+ (getf (first ts) :duration)
	      (add-matching-entries-duration (rest ts))))))

(defun collapse-matching-entries-one-entry (ts)
  (let ((place (getf (first ts) :place))
	(soll (getf (first ts) :soll))
	(duration
	  (add-matching-entries-duration
	   (find-same-entries-one-compacted-ts ts))))
    (list :place place :soll soll :duration duration)))

(defun remove-entry-from-compacted-ts (entry ts)
  (cond ((null ts) nil)
	((and (eq (getf entry :place)
		  (getf (first ts) :place))
	      (eq (getf entry :soll)
		  (getf (first ts) :soll)))
	 (remove-entry-from-compacted-ts entry (rest ts)))
	(t (cons (first ts)
		 (remove-entry-from-compacted-ts entry (rest ts))))))

(defun collapse-matching-entries-all-entries (ts)
  "Given a list of compacted ENTRY, collapse matching entries."
  (cond ((null ts) nil)
	(t (cons
	    (collapse-matching-entries-one-entry ts)
	    (collapse-matching-entries-all-entries
	     (remove-entry-from-compacted-ts (first ts) ts))))))

;; AGGREGATE ENTRIES ACCORDING TO TYPE
;; ==================================================================================

(defun remove-empty-entries (lst)
  (let ((one (first lst)))
    (cond ((null lst) nil)
	  ((when one
	     (cons one (remove-empty-entries (rest lst)))))
	  (t (remove-empty-entries (rest lst))))))

(defun aggregate-entries-matching-type (type lst)
  (let ((one (first lst))
	(keyword (intern (string-upcase type) "KEYWORD")))
    (cond ((null lst) nil)
	  ((consp (first one))
	   (append (first (getf one keyword))
		   (aggregate-entries-matching-type keyword
						    (rest lst))))
	  (t (append (getf one keyword)
		     (aggregate-entries-matching-type keyword (rest lst)))))))


;; SYNTHESIZE COLLAPSING AND AGGREGATING
;; ==================================================================================

(defun aggregate-and-collapse-entries (start end type array)
  (let ((entries
	  (remove-empty-entries
	   (get-list-of-entries-year-array start end array))))
    (collapse-matching-entries-all-entries
     (aggregate-entries-matching-type type entries))))

;; BUILD MONTH, QUARTER, YEAR RECORDS
;; ==================================================================================

(defun construct-month (month array)
  (let ((start (calculate-day-in-year (list 01 month)))
	(end   (- (calculate-day-in-year (list 01 (+ month 1))) 1)))
    (construct-timespan
     start
     end
     nil
     nil
     (aggregate-and-collapse-entries start end 'pk array)
     (aggregate-and-collapse-entries start end 'ov2 array)
     (aggregate-and-collapse-entries start end 'ov1 array))))

(defun construct-quarter (quarter array)
  (let* ((q-start (- (* quarter 3) 2))
	 (q-end   (+ q-start 3))
	 (start   (calculate-day-in-year (list 01 q-start)))
	 (end     (- (calculate-day-in-year (list 01 q-end)) 1)))
    (construct-timespan
     start
     end
     nil
     nil
     (aggregate-and-collapse-entries start end 'pk array)
     (aggregate-and-collapse-entries start end 'ov2 array)
     (aggregate-and-collapse-entries start end 'ov1 array))))

(defun construct-year (array)
  (let ((start 0)
	(end *current-days-in-year*))
    (construct-timespan
     start
     end
     nil
     nil
     (aggregate-and-collapse-entries start end 'pk array)
     (aggregate-and-collapse-entries start end 'ov2 array)
     (aggregate-and-collapse-entries start end 'ov1 array))))
