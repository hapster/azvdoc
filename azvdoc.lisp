(defpackage :azvdoc
  (:nicknames :azv)
  (:use #:cl #:clog #:clog-gui)
  (:export start-app)
  ;; (:export #:set-current-date-variables
  ;;	   #:set-current-month-days-alist-variable
  ;;	   #:compute-hours
  ;;	   #:get-current-date
  ;;	   #:calculate-day-in-year
  ;;	   #:write-array-to-file
  ;;	   #:read-file
  ;;	   #:transform-timespans-to-day
  ;;	   #:set-one-day-year-array
  ;;	   #:get-list-of-entries-year-array
  ;;	   #:aggregate-and-collapse-entries
  ;;	   #:construct-timespan
  ;;	   #:construct-month
  ;;	   #:construct-quarter
  ;;	   #:construct-year
  ;;	   #:*current-date*)
  )

(in-package :azvdoc)

(defclass app-data ()
  ((data
    :accessor data)))

(defun on-input-simple (obj)
  (let* ((app (connection-data-item obj "app-data"))
	 (win (create-gui-window obj :title "schnelle Eingabe"
				     :width 425
				     :height 325)))
    (declare (ignore app))
    (create-input-simple (window-content win))))

(defun change-date-format-us-de (date-string)
  "Turn US-formatted date 'yyyy-mm-dd' into DE-formatted date 'dd.mm.yyyy'."
  (let* ((split-date (uiop:split-string date-string :separator "-"))
	 (year (first split-date))
	 (month (second split-date))
	 (day (third split-date)))
    (concatenate 'string day "." month "." year)))

(defun turn-de-date-string-to-date-list (date-string)
  "Turn DE-formatted date \"dd.mm.yyyy\" into date list with symbols (dd mm yyyy)."
  (let ((split-date (uiop:split-string date-string :separator ".")))
    (mapcar 'parse-integer split-date)))

(defun input-simple-preview (panel)
  (let* ((pk (text-value (pk-input panel)))
	 (ov2 (text-value (ov2-input panel)))
	 (ov1 (text-value (ov1-input panel)))
	 (date-wrong-format (value (date-picker panel)))
	 (date (change-date-format-us-de date-wrong-format))
	 (date-day-in-year (write-to-string
			    (calculate-day-in-year
			     (turn-de-date-string-to-date-list date)))))
    (confirm-dialog panel
		    (format nil "Richtig so? ~&~A"
			    (concatenate 'string
					 "Datum: " date " "
					 "pK: " pk " Std. "
					 "OV2: " ov2 " Std. "
					 "OV1: " ov1 " Std. "))
		    (lambda (answer)
		      (if answer 'yes 'no))
		    :title "Eingabe prüfen"
		    :ok-text "Fertig"
		    :cancel-text "Zurück")))

(defun on-new-window (body)
  (let ((app (make-instance 'app-data)))
    (setf (connection-data-item body "app-data") app)
    (setf (title (html-document body)) "azvdoc")
    (clog-gui-initialize body)
    (add-class body "w3-teal")
    (let* ((menu-bar    (create-gui-menu-bar body))
	   (input-dropdown (create-gui-menu-drop-down menu-bar :content "Eingabe"))
	   (input-simple-item (create-gui-menu-item input-dropdown :content "Einfach"
								   :on-click 'on-input-simple))
	   ;; (icon-item   (create-gui-menu-icon menu-bar :on-click 'on-help-about))
	   ;; (file-item   (create-gui-menu-drop-down menu-bar :content "File"))
	   ;; (file-new    (create-gui-menu-item file-item :content "New Window" :on-click 'on-file-new))
	   ;; (help-item   (create-gui-menu-drop-down menu-bar :content "Help"))
	   ;; (help-about  (create-gui-menu-item help-item :content "About" :on-click 'on-help-about))
	   (full-screen (create-gui-menu-full-screen menu-bar))
	   )
      (declare (ignore input-simple-item full-screen)))))

(defun start-app ()
  (initialize 'on-new-window
   :static-root (merge-pathnames "./www/"
		  (asdf:system-source-directory :azvdoc)))
  (open-browser))
