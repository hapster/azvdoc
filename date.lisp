(cl:in-package :azvdoc)

(ql:quickload "local-time")

;; Functions related to date and its manipulations
;; ========================================================================
(defparameter *month-days-alist*
  '((01 . 31)
    (02 . 28)
    (03 . 31)
    (04 . 30)
    (05 . 31)
    (06 . 30)
    (07 . 31)
    (08 . 31)
    (09 . 30)
    (10 . 31)
    (11 . 30)
    (12 . 31))
  "Number of days per months in an alist for regular year.")

(defparameter *month-days-leap-alist*
  '((01 . 31)
    (02 . 29)
    (03 . 31)
    (04 . 30)
    (05 . 31)
    (06 . 30)
    (07 . 31)
    (08 . 31)
    (09 . 30)
    (10 . 31)
    (11 . 30)
    (12 . 31))
  "Number of days per months in an alist for a leap year.")

(defparameter *month-num-to-month-name-alist*
  '((01 . "Januar")
    (02 . "Februar")
    (03 . "März")
    (04 . "April")
    (05 . "Mai")
    (06 . "Juni")
    (07 . "Juli")
    (08 . "August")
    (09 . "September")
    (10 . "Oktober")
    (11 . "November")
    (12 . "Dezember"))
  "Month number to month name alist.")

(defvar *recent-day* nil "The last 'current' value for day.")
(defvar *recent-month* nil "The last 'current' value for month.")
(defvar *recent-year* nil "The last 'current' value for year.")

(defvar *current-date* nil "Current date as three integer numbers in a list.")
(defvar *current-day* nil "Current day as integer number.")
(defvar *current-month* nil "Current month as integer number.")
(defvar *current-quarter* nil "Current quarter as integer number.")
(defvar *current-year* nil "Current year as integer number.")
(defvar *current-month-days-alist* nil
  "Month days alist depending on (no) leap year.")

(defvar *current-days-in-year* nil "Number of days in current year.")

(defvar *array-index-numbers* nil
  "Keep track of the DAY-NUMBERS input to the YEAR-ARRAY.")
(defvar *array-entry-count* (length *array-index-numbers*)
  "Keep track of the number of entries in the YEAR ARRAY.")

(defun leap-year-p (year)
  "Boolean that returns T on leap year, else NIL."
  (or (integerp (/ year 400))
      (and (integerp (/ year 4))
	   (not (integerp (/ year 100))))))

(defun set-current-month-days-alist-variable (year)
  "Export months-days-alist depending on leap or normal year."
  (if (leap-year-p year)
      (setf *current-month-days-alist*
	    *month-days-leap-alist*)
      (setf *current-month-days-alist*
	    *month-days-alist*)))

(defun get-current-date ()
  "Current date in (DD MM YYYY) format."
  (let ((today (local-time:now)))
    (list (local-time:timestamp-day today)
	  (local-time:timestamp-month today)
	  (local-time:timestamp-year today))))

(defun set-current-date-variables ()
  (let* ((today    (get-current-date))
	 (leap     (leap-year-p (first (last today))))
	 (number   (if leap 366 365)))
    (setf *current-day* (first today))
    (setf *current-month* (second today))
    (setf *current-year* (third today))
    (setf *current-days-in-year* number)
    (setf *current-date* today)))

(defun calculate-number-of-days (alist start end)
  "Number of elapsed days between START and END month."
  (cond ((or (eq (+ 1 end) start)
	     (null alist)) 0)
	(t (+ (cdr (assoc start alist))
	      (calculate-number-of-days (cdr alist)
					     (+ 1 start)
					     end)))))

(defun calculate-day-in-year (date)
  "Number of elapsed days since first day of year."
  (let ((days (first date))
	(month (second date)))
    (+ (calculate-number-of-days *current-month-days-alist*
				      1
				      (- month 1))
       (- days 1))))
