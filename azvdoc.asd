(asdf:defsystem #:azvdoc
  :description "[A]rbeis[Z]eit[V]erteilungs-[DOC]umentation"
  :author "Olivier Rojon"
  :license "GPLv3"
  :version "0.0.0"
  :serial t
  :depends-on (#:clog
	       #:local-time
	       ;; #:cl-typesetting
	       )
  :components ((:file "azvdoc")
	       (:file "input-simple")
	       (:file "date")
	       (:file "time")
	       (:file "init")
	       (:file "save-restore")
	       (:file "construct-transform")))
