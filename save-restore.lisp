(cl:in-package :azvdoc)

(defun write-array-to-file (array file)
  "Write given ARRAY to FILE."
  (with-open-file (out file
		       :direction         :output
		       :if-does-not-exist :create
		       :if-exists         :supersede)
    (with-standard-io-syntax
      (print array out))))

(defun read-file-to-array (file array)
  "Read FILE, save contents to ARRAY.
Is used to supply the ARRAY contents to initialise *CURRENT-YEAR-ARRAY*."
  (with-open-file (in file
		      :if-does-not-exist :error)
    (with-standard-io-syntax
      (setf array (read in)))))

(defparameter *current-year-array-file* nil
  "This file holds the user-input data, i.e. the working times and their distribution.
It is used as input to *CURRENT-YEAR-ARRAY*.")

(defparameter *current-year-array* nil
  "This array holds the day entries for the current year.")

(defparameter *default-storage-location* nil
  "Data such as the YEAR-ARRAY-FILE will be stored here.")
