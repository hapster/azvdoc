# AZVdoc - Dokumentation der ArbeitsZeitVerteilung

Mit AZVdoc wird es leicht, die eigene Arbeitszeit und deren Verteilung im Blick zu
behalten und in übersichtlicher Form auszugeben.  Das Programm befindet sich im
pre-alpha Stadium ([Status](https://gitlab.con/hapster/azvdoc#status)) und es gibt
noch kein Release ([Hinweise zur
Installation](https://gitlab.con/hapster/azvdoc#installation)).  **Die
[Übersicht](https://gitlab.com/hapster/azvdoc#übersicht) beschreibt das Design des
Programms, nicht seine aktuelle Funktionalität.**

**An english version of the manual will follow as soon as both the program and its
documentation have matured sufficiently to target a wider audience.**

## Übersicht

### Ratio

AZVdoc ist ein Programm mit grafischer Oberfläche, mit dem die eigene Arbeitszeit und
deren Verteilung nachvollziehbar wird.  Das Programm wird entwickelt, um in der
pädagogischen Arbeit überprüfen zu können, wie sich die Arbeit zwischen den Posten
"pädagogische Kontaktzeit", "Vor- und Nachbereitung pädagogischer Angebote" und
"Verwaltung und Sonstiges" aufteilt.

Durch diese Dokumentation der Arbeitszeit-Verteilung soll einerseits die Bilanzierung
der pädagogischen Angebote am Ende des Jahres leichter von der Hand gehen und sollen
sich andererseits Trends in der Verteilung der Arbeitszeit (z.B. zu viel/zu wenig
pädagogische Kontaktzeit) identifizieren lassen.

Aktuell beschränkt sich das Anwendungsgebiet auf die pädagogische Arbeit des Autors.
Das Programm ist aber so konzipiert, dass sich der Anwendungsbereich auch
nachträglich noch erweitern lässt.

**AZVdoc entsteht als Hobby-Projekt.**  Das Projekt dient dem Erlernen der
Programmiersprache Common Lisp und verwandter Technologien sowie der Dokumentation
meiner eigenen Arbeitszeit.  Erlangt das Programm hinreichende Nutzbarkeit, so soll
es in Absprache mit Vorgesetzten zur freiwilligen Verwendung an meine Kolleg:innen
ausgegeben werden.

<!-- Hier wären Screenshots oder eine Demo gut! -->
<!-- ### Screenshots -->

### Technologien

AZVdoc ist in der Multiparadigmen-Programmiersprache Common Lisp geschrieben.  Für
die grafische Oberfläche wird das Framework
[CLOG](https://github.com/rabbibotton/clog) verwendet, welches ebenfalls in Common
Lisp geschrieben ist und mit der Websocket-Technologie arbeitet.

### Grafische Oberfläche

Die grafische Oberfläche besteht aus drei Hauptbestandteilen:
- Im Eingabe-Fenster werden Angaben zur Arbeitszeit und deren Verteilung gemacht.
Die Eingabe erfolgt per Tastatur oder Maus und die Angaben können unterschiedlich
ausführlich sein.  Je mehr Angaben gemacht werden, desto genauer wird natürlich die
Ausgabe.
- Im Ausgabe-Fenster werden die früher gemachten und lokal gespeicherten Angaben nach
Wunsch für unterschiedliche Zeitfenster zusammengetragen und ausgegeben.  Mögliche
Zeitfenster sind: Tag, Woche, Monat, Quartal, Jahr.  Die Daten werden sowohl mit
Tabellen als auch mit Diagrammen zugänglich gemacht.
- Im Export-Fenster können die gemachten Angaben für die jeweiligen Zeitfenster im
PDF-Format ausgegeben werden.

## Status

Das Programm ist aktuell im pre-alpha Stadium.  Einige grundlegende Funktionen sind
bereits geschrieben, aber wesentliche Aspekte der Kernfunktionalität sind noch nicht
gegeben.

### Was funktioniert

- Eine stark reduzierte Version der grafischen Oberfläche.  Hier kann eine Eingabe
  simuliert und können die Eingaben überprüft werden (die Übertragung von grafischer
  Oberfläche zu "meinen" Funktionen ist noch nicht implementiert).
- Die Datenstruktur für eine Zeitspanne (z.B. 10:00 -- 12:00 inklusive Angaben zu
  Arbeitsort und Tätigkeitstyp).  Mehrere Zeitspannen eines Tages können zu einem
  "Tag" zusammengesetzt werden.
- Eine simple Datenbank-Struktur: ein Array mit so vielen Slots, wie das Jahr Tage
  hat (berücksichtigt Schaltjahre).  Hier werden die "Tage" gespeichert.
- Erkennen, der wievielte Tag im Jahr das heutige Datum (oder ein beliebiges anderes)
  ist.  So können die Eingaben für einen Tag im entsprechenden Slot des Array
  eingespeichert werden.
- Die im Array gespeicherten Angaben zu größere Zeitspannen wie "Monat", "Quartal"
  und "Jahr" zusammensetzen und ausgeben.
- Das Speichern des Array in einer Datei und das Auslesen einer Datei, die einen
  Array enthält.

### TODO / Entwicklungsweg
- [ ] Eine neue Datenstruktur, gegebenenfalls als SQL-Datenbank
- [ ] Visualisierung der Eingaben durch Tabellen und Diagramme
- [ ] Export der Tabellen und Diagramme für eine bestimmte Zeitspanne im PDF-Format
- [ ] Implementierung der grafischen Oberfläche
  - [ ] Eingabefenster, bedienbar per Tastatur und Maus
  - [ ] Ausgabefenster, in dem die Daten für unterschiedliche Zeitspannen angezeigt
        werden
  - [ ] Exportfenster
- [ ] Einstellungen, über die Parameter des Programms gesteuert werden können
      (z.B. Ablage-Ort für die Datenbank, bevorzugtes Startfenster,
      Standard-Export-Optionen)
- [ ] Verbesserte Routine zur Programminitialisierung
- [ ] Tendenzielle Immunität gegen Fehler unterschiedlicher Art
- [ ] Ausführbare Datei, die auf den geläufigen Plattformen funktioniert (Apple,
      Windows, Linux)
	  
## Installation

Wie weiter oben beschrieben wurde, gibt es aktuell kein Release, also auch keine
einfach ausführbare Datei.  Das Programm wurde bisher ausschließlich auf Linux
getestet.  **Es gibt also keine Garantie, dass die unten beschriebenen Schritte auf
anderen Plattformen genauso anwendbar sind!**

### Voraussetzungen

- Eine Common Lisp-Implementierung (geschrieben und getestet in Steel Bank Common
  Lisp [SBCL])
- Quicklisp ([Installationsanweisungen](https://www.quicklisp.org/beta/))
- Den Code aus diesem Repository in einem speziellen Verzeichnis, sodass das Programm
  gefunden wird: `$HOME/common-lisp/azvdoc` (entscheidend ist `$HOME/common-lisp`,
  nicht der Unterordner)

### Start des Programms

- Öffnen eines Terminals
- Starten des Lisp-REPLs, z.B. `sbcl`
- Im REPL: `(ql:quickload :azvdoc)` (lädt die Abhängigkeiten von AZVdoc herunter),
  und dann `(azv:start-app)`.
  
Es öffnet sich dann ein neues Browser-Tab, in dem die minimalistische grafische
Oberfläche des Programms angezeigt wird ([das Programm ist im pre-Alpha
Stadium](https://gitlab.com/hapster/azvdoc#status)).

## License
BSD
