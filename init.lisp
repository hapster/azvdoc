;; INITIALISATION
;; ========================================================================

(cl:in-package :azvdoc)

(set-current-date-variables)
(set-current-month-days-alist-variable *current-year*)

;; Here I will need something to load a saved array from file
;; Also I will need to update the number of array entries
