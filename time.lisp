;; Collapse time spans
;; ========================================================================
;; (10 00 11 15) => (1,25) ; (22 15 01 35) => (3,5)

(cl:in-package :azvdoc)

(defun compute-duration (list)
  "Given a two time span LIST (HH MM HH MM), compute duration."
  (let* ((start-hours (first list))
	 (end-hours (third list))
	 (hours (if (> start-hours end-hours)
		    (+ (- 24 start-hours) end-hours)
		    (- end-hours start-hours)))
	 (mins (- (fourth list) (second list)))
	 (deci-mins (* (/ mins 60) 1.0))
	 (time-span (+ hours deci-mins)))
    (if (> time-span 0)
	time-span
	(* -1 time-span))))

(defun separate-components (timestamp)
  "Given a timestamp STRING \"HH:MM\", separate components (HH MM)."
  (mapcar #'parse-integer
	  (list (subseq timestamp 0 2)
		(subseq timestamp 3 5))))

(defun compute-hours (start end)
  "Given two timestamp STRINGS \"HH:MM\", compute hours."
  (compute-duration
   (append (separate-components start)
	   (separate-components end))))
